provider "aws" {
  region = "us-east-2"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name = "strongdm-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-2a", "us-east-2b", "us-east-2c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = false

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

resource "aws_security_group" "unicorn-workers-sg" {
  name = "unicorn-workers-sg"
  vpc_id = module.vpc.vpc_id
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_launch_configuration" "lc-unicorn-workers" {
  image_id          = "ami-0c7a85a533e87e6f6"
  instance_type = "t2.nano"
  security_groups = [aws_security_group.unicorn-workers-sg.id]
  
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "elb-sg" {
  name = "elb-sg"
  vpc_id = module.vpc.vpc_id
  # Allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Inbound HTTP from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_autoscaling_group" "asg-unicorn-workers" {
  launch_configuration = aws_launch_configuration.lc-unicorn-workers.id
  vpc_zone_identifier= module.vpc.private_subnets.*
  min_size = 2
  max_size = 5

  load_balancers    = [aws_elb.unicorn-elb.name]
  health_check_type = "ELB"

  tag {
    key                 = "Name"
    value               = "unicorn-workers ASG"
    propagate_at_launch = true
  }
}

resource "aws_elb" "unicorn-elb" {
  name               = "unicorn-elb"
  security_groups    = [aws_security_group.elb-sg.id]
  subnets = module.vpc.public_subnets.*
  # availability_zones = module.vpc.azs

  health_check {
    target              = "HTTP:8080/"
    interval            = 30
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  # Adding a listener for incoming HTTP requests.
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }
}
