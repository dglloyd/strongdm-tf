variable "name" {
  default     = "packer"
  description = "The name of the image build environment"
  type        = string
}

#
variable "region" {
  default     = "us-east-2"
  description = "The AWS region to deploy to"
  type        = string
}

#
variable "vpc_cidr_prefix" {
  default     = "10.72"
  description = "The IP prefix to the CIDR block assigned to the VPC"
  type        = string
}

variable "ami_id" {
  default = "ami-056d85e91fa9e4b82"
  type    = string
}
