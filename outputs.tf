output "vpc_id" {
  value       = aws_vpc.packer.id
  description = "Packer VPC ID"
}

output "subnet_id" {
  value       = aws_subnet.packer.id
  description = "Packer Subnet ID"
}

output "elb_dns_name" {
  value       = aws_elb.unicorn-elb.dns_name
  description = "ELB FQDN for unicorn service"
}
